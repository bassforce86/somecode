---
title: Nexusmods
from: 2018-08-01T00:00:00+00:00
to: 2021-10-01T00:00:00+00:00
job: Ruby on Rails Developer
weight: 2
tags:
- ruby
- ruby on rails
- go
- gitlab
- git
- php
- javascript
- ci/cd
- kubernetes
- docker
- nginx
- linux
- postgresql
- programming
---

## Brief overview of duties

Develop, Test & Release Service Workers, Webapps and API’s both in Open source and Closed source communities. Mainly working in Ruby (Ruby on Rails) & Golang, deploying via Gitlab to Kubernetes. Strong focus on High Availability development based on the 12 factor app principles as well as utilising Digital Ocean & GCP.

## Skills

- Strong knowledge of Ruby on Rails (RoR)
- Strong knowledge of Docker (including docker-compose)
- Strong knowledge of Linux (Debian / Ubuntu / Fedora)
- Working knowledge of Golang
- Working knowledge of C#
- Good knowledge of PostgresQL
- Strong knowledge of Git (Gitlab & Github)
