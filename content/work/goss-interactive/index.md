---
title: Goss Interactive
from: 2016-06-01T00:00:00+00:00
to: 2018-08-01T00:00:00+00:00
job: .NET & Java Developer
weight: 3
tags:
- c-sharp
- java
- .net framework
- .net core
- mysql
- git
- svn
- jenkins
- customer service
- business relations
- windows
- programming
---

## Brief overview of duties

Develop, Test & Release Websites built using the .NET Framework. Deployment using Jenkins, Project Updates, Bug fixes and Client Communications through JIRA, and Managing code through SVN.

## Skills

- Strong knowledge of the .NET Framework
- Strong knowledge of C#
- Good knowledge of Java, HTML, CSS and JS.
- Working knowledge of SQL.
- Excellent Problem Solving skills
- Strong organisational skills
- Strong knowledge of Version Control Systems (GIT, SVN etc)
- Good knowledge of Jenkins Build Servers
- High quality customer service standards
- Strong understanding of Business practices.
