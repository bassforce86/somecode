---
title: Computing, Networking & Software Development
level: 
  name: Foundation Degree
  short: FD
location: Cornwall College, UK
from: 2011-09-12T00:00:00+00:00
to: 2014-06-12T00:00:00+00:00
result: Pass
tags:
- asp.net
- HTML
- CSS
- Javascript
- PHP
- Postgres
- MySQL
- Cisco
- CCNA
- CCNS
- Networking
- HCI
- Project Management
- Security
- Web Development
- programming
weight: 2
---

## Course Description

The programme offers you a common first year with the opportunity to specialise in software development or computer networking in the second year. The content for Cisco networking qualifications are embedded within your foundation degree which will help you to gain employment on completion of your programme. We have liaised closely with local industry to identify local needs and the Sector Skills Council, e-skills, to identify national trends in demand.

The programme is broad-based, giving a good grounding in current practice and developments. You have the opportunity to pursue areas of special interest, through optional modules, to focus on computer networking or software development, together with your integrated project undertaken in Year 2.

During the foundation degree, you will also study towards the Cisco Certified Network Associate and Cisco Certified Security content. This complementary study will prepare you to undertake the industry recognised awards of CCNA and CCNS which will provide you with a unique qualification, putting you in a strong position for future employment or career progression.
