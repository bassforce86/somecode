---
title: Veedubmachine
from: 2013-02-01T00:00:00+00:00
to: 2015-09-01T00:00:00+00:00
job: IT Operations Manager
weight: 5
tags:
- Marketing
- Customer service
- support
- hardware
- systems
- sales
- cisco

---

## Brief overview of duties
Handle company wide strategy for all technology within the company. Including Point of Sale, Digitizing Inventory Management, Sales platforms as well as in-house VOIP and Networking hardware.

## Skills
- Working knowledge of Public Relations including Marketing, Sales and Advertising.
- Strong, intricate, IT knowledge.
