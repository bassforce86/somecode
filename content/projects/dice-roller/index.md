---
date: "2020-05-07"
draft: false
tags:
- programming
- javascript
- js
- dnd
- dice
- discord-bot
- node
title: Discord Dice Roller Bot
---

Quick Project for a Dungeons and Dragons group I am part of.

self-hostable discord bot for D&D 5e dice rolls with a readable embed response.

- [GitLab Repo](https://gitlab.com/bassforce86/dice-roll-discord)
