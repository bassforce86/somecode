---
date: "2020-06-13T16:57:54+01:00"
draft: false
tags:
- programming
- js
- javascript
- stream
- monitor
- stadia
- twitch
- youtube
- chrome-extension
title: Stream Monitor
---

Here's a quick overview of the Stream Monitor project!

Stream Monitor is a Chrome Extension that shown some basic monitors for HTML5 video streams.
Currently supporting Stadia, YouTube & Twitch, with plans to expand beyond that and support many other platforms.

To find out mode head on over to [stream-monitor.somecode.dev](https://stream-monitor.somecode.dev).

- Community Discord: [Stream Monitor](https://discord.gg/JHT8A8u)
- Support me and this project: [Ko-fi](https://ko-fi.com/bassforce86)
