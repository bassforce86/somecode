---
title: Carillon Audio Systems
from: 2009-02-01T00:00:00+00:00
to: 2016-10-01T00:00:00+00:00
job: Regional Technical Manager
weight: 4
tags:
- Marketing
- Customer service
- support
- hardware
- systems
- sales
---

## Brief overview of duties
Technical support encompassing frontline through to high level support, building bespoke studio computer systems, on-call customer relations and on-site installations

## Skills
- Working knowledge of Public Relations including Marketing, Sales and Advertising.
- Strong, intricate, IT and Music knowledge.
