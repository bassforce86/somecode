---
date: "2015-04-22"
draft: false
tags:
- programming
- games
- js
- itch.io
- phaser
- p2
- javascript
title: Free Skate
---

This was a University project for my Web-based Games module.

Using [Phaser.js](http://phaser.io/), this little skate game was made to attempt to bring some semblance of actual physics to this little JS game.

It's still a fun little run to make and won't see any more love from me, but I've come a long way since it was built and make make a new version some time down the line when I feel I have the time to dedicate to doing it properly.

The game can be found on [itch.io](http://itch.io) under the name [free-skate](https://bassforce86.itch.io/free-skate). 
