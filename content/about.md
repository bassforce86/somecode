---
comments: false
subtitle: The truth, the whole truth and everything except the truth
date: 1986-09-24T00:00:00+00:00
tags:
- MTB
- music
- developer
- ruby
- go
- PHP
- c-sharp
- docker
- kubernetes
title: About me
---

I’m a friendly and approachable character, with a drive to help others and keep up with the latest digital technologies and advancements. I work predominantly in Golang. I have strong networking and communication skills, with a natural ability to solve problems. Alongside this, I am a Computer Hardware Technician, with strong ties to the music industry, currently exploring ways of embedding this into my programming and development.

Outside of work, I play some guitar, but considerably better on Electric bass. I'm a mountain biker, so most weekends I'm riding out in the forests. I play D&D, love Motorsport and have an uncanny ability to "ruin" songs... mostly by intentionally miss-quoting them.

## Hobbies
- Making Music [producer, musician, engineer]
- Getting on my Bike [off road wherever possible]
- Playing Games [mostly adventure & racing, both arcade and sim]

## Interesting Things
- I enjoy my profession
- I fix more things than I break

## Achievements
During my time playing with various bands, I've had the privilege to share stages with the likes of:
- Status Quo
- Quireboys
- Deborah Bonham
- Reef

Recorded artists & been Recorded in many studios from the likes of:
- Sawmills
- Cube Recording
- "The Van"
