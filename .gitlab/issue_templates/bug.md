# Bug Report
## Summary

(Summarize the bug encountered as concisely as you can)


## Steps to reproduce

(How can we reproduce the issue? - this is very important)


## What is the current bug behaviour?

(What actually happens?)


## What is the expected correct behaviour?

(What you should happen?)


## Relevant logs and/or screenshots

(Paste any relevant logs - please use code blocks (```) to format console output,
logs, and code as it's very hard to read otherwise.)


## Possible fixes

(If you can, link to the line of code that might be responsible for the problem)

/label ~bug

