---
title: Secondary Education
level: 
  name: Secondary Education
  short: GCSE
location: Penair, Truro, UK
from: 1997-09-12T00:00:00+00:00
to: 2003-06-12T00:00:00+00:00
tags:
- maths
- biology
- chemisty
- physics
- english
- it
- design tech
- art
- drama
weight: 5
---

### Results

Maths
: C

Science
: D

English
: C

Systems & Control (Design Technology)
: C

Drama
: C

German
: D

ICT (GNVQ)
: Pass