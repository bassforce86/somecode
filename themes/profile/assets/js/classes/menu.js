export class Menu {
  constructor(el) {
    this.element = document.getElementById('navbar') || el;
    this.icon = document.getElementById("menu-icon");
    this.state = this.CLOSED;
    this.load;
  }

  get OPEN() { return "open"; }
  get CLOSED() { return "closed"; }

  get states() {
    return [this.OPEN, this.CLOSED];
  }
  
  toggle() {
    this.element.classList.remove(...this.element.classList);
    if (this.state == this.CLOSED) {
      this.switch(this.OPEN)
    } else {
      this.switch(this.CLOSED)
    }
  }

  switch(state) {
    this.element.classList.remove(...this.element.classList);
    this.icon.classList.remove(...this.icon.classList);
    if (state && this.states.includes(state)) {
      switch (state) {
        case this.OPEN:
          this.state = this.OPEN;
          this.icon.classList.add(this.state);
          break;
        default:
          this.state = this.CLOSED;
      }
    }
    this.element.classList.add(this.state);
  }
}
