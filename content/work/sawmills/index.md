---
title: Sawmills Studios
from: 2009-02-01T00:00:00+00:00
to: 2010-06-01T00:00:00+00:00
job: Sound Technician
weight: 6
tags:
- Professional Audio
- Customer service
- Audio Engineering
- Computer Hardware
- Music Industry
- Live Sound
- Mixing
- Mastering
---

## Brief overview of duties
In-house Sound Engineer, which included Room Prep, Mic placement & setup, cable maintenance and cleaning down.

## Skills
- Strong People Management skills
- Strong Time Management
- Intricate knowledge of microphone dynamics and acoustics
- Strong knowledge of Sound
