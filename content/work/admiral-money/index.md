---
title: Admiral Money
from: 2021-10-18T00:00:00+00:00
to:
job: Tech Lead / SRE / SSE
weight: 1
tags:
- terraform
- terragrunt
- concourse
- drone
- go
- gitlab
- python
- ci/cd
- kubernetes
- docker
- ECR
- EKS
- ECS
- Lambda
- Route-53
- git
- git-ops
- programming
- SSE
- SRE
---


## Brief overview of duties

Develop, Test & Release Service Workers and API’s. Mainly working in Golang, deploying via Gitlab. Strong focus on High Availability development based on the 12 factor app principles.

Lead a team of highly capable engineers in a cohesive and collaborative environment, working alongside Architects and Business Analysts to provide a strong, progressive approach to our Agile team and underlying standards.

My role has expanded over my time here to also officially encompass DevOps responsibilities. This means I have a split focus on maintainng high quality standards for both our Engineering teams and Platform teams. We make heavy use of terraform, terragrunt, EKS (K8s), and IAM.

&nbsp;

## Skills
- Strong knowledge of Golang
- Strong knowldge of Kubernetes
- Strong knowledge of Docker (including docker-compose)
- Strong knowledge of Linux (Debian / Alpine)
- Good knowledge of AWS


## Glossary
 - **SRE**: Site Reliability Engineer
 - **SSE**: Senior Software Engineer
 - **Tech Lead**: Technical Lead
