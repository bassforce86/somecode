---
title: Computing & Game Development
level: 
  name: Bachelor of Science (Honours)
  short: BSc Hons
location: Plymouth University, UK
from: 2014-09-12T00:00:00+00:00
to: 2016-06-12T00:00:00+00:00
result: 1st Class Honours
tags:
- C-sharp
- Java
- CUDA
- C++
- Business
- Animation
- Games
- Unity3D
- Unreal Engine
- Web Development
- programming
weight: 1
---

## Course Description

Create your own apps, indie releases and serious games and build your profile as a software developer. This course will challenge you to innovate in the games sector while honing your software development skills and working for real clients on live projects. Work individually and as part of a team, prove your capabilities with tech demos and releases. Our course is built on a core of computer science topics, supported by industry veterans and our own in-house Interactive Systems Studio. 
