---
date: "2019-10-10T18:34:00+01:00"
draft: false
tags:
- programming
- scss
- css
- vortex
- theme
- skin
- nexusmods
title: VUIX
---

This is a simply re-skin for [Vortex](https://www.nexusmods.com/about/vortex/) Mod Manager.

If you want to see what it looks like you can veiw it on: [Nexusmods](https://www.nexusmods.com/site/mods/46) 

or see the source on: [GitLab](https://gitlab.com/bassforce86/vuix)
