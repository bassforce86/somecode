---
title: Swim Teacher
level:
  name: Level 1 & 2
  short: ASA
location: Nationwide, UK
from: 2007-10-02T00:00:00+00:00
result: Pass
tags:
- swimming
- teacher
- coach
- adults
- children
weight: 3
---

## ASA Swimming Teacher Levels 1 & 2

- Core Theory
- Swimming Theory
- Practical Teaching - Non-swimmer to Advanced 1
- Practical Teaching - Advanced 2 to Pre-Competition
