export class Theme {
  constructor(el) {
    this.element = document.body || el;
    this.button = document.getElementById("theme-toggle");
    this.load;
  }

  get DARK_THEME() { return "dark"; }
  get LIGHT_THEME() { return "light"; }
  get DARK_ICON() { return `<i class="fas fa-sun fa-sm"></i> Theme`; }
  get LIGHT_ICON() { return `<i class="fas fa-moon fa-sm"></i> Theme`; }

  get skins() {
    return [this.LIGHT_THEME, this.DARK_THEME];
  }
  
  get load() {
    let theme = localStorage.getItem("theme");
    if (theme && theme !== this.skin) {
      this.switch(theme);
    } else {
      this.skin = this.DARK_THEME;
    }
    console.log(`theme loaded: ${this.skin}`);
    this.save;
  }

  get save() {
    this.element.classList.add(this.skin)
    localStorage.setItem("theme", this.skin);
    console.log(`theme updated: ${this.skin}`);
  }

  toggle() {
    this.element.classList.remove(...this.element.classList);
    if (this.skin == this.LIGHT_THEME) {
      this.switch(this.DARK_THEME)
    } else {
      this.switch(this.LIGHT_THEME)
    }
    this.save;
  }

  switch(skin) {
    this.element.classList.remove(...this.element.classList);
    if (skin && this.skins.includes(skin)) {
      switch (skin) {
        case this.DARK_THEME:
          this.skin = this.DARK_THEME;
          this.button.innerHTML = this.DARK_ICON;
          break;
        default:
          this.skin = this.LIGHT_THEME;
          this.button.innerHTML = this.LIGHT_ICON;
      }
    }
  }
}
