---
title: Pool Lifeguard
level:
  name: National Pool Lifeguard Qualification
  short: NPLQ
location: Nationwide, UK
from: 2004-05-26T00:00:00+00:00
result: Pass
tags:
- lifesaving
- swimming
- first aid
- search and rescue
- supervision
weight: 4
---

## Award Description

The NPLQ is the most awarded lifeguard qualification in the UK and Ireland and is also internationally recognised. More than 40,000 pool lifeguards qualify with the RLSS UK every year, and there are currently more than 80,000 pool lifeguards qualified in the NPLQ – 95% of all UK pool lifeguards.

## Lifeguard Course Pre-Requisites:

- Jump/dive into deep water
- Swim 50 metres in less than 60 seconds
- Swim 100 metres continuously on front and back in deep water
- Tread water for 30 seconds
- Surface dive to the floor of the pool
- Climb out unaided without ladder/steps and where the pool design permits
